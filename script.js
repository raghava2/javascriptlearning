/**
 * Create a class for the Backpack object type.
 * @link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes
 */
import Backpack from "./Backpack.js";
import Book from "./Book.js";

const everydayPack = new Backpack(
  "Everyday Backpack",
  30,
  "grey",
  15,
  26,
  26,
  false
);

console.log("The everydayPack object:", everydayPack);
console.log("The pocketNum value:", everydayPack.pocketNum);
// console.log(b1);

console.log("rhv");
const b1 = new Book(true, 123, "Sherlock Holmes", "Best book of the year");

console.log("Rgv names");
console.log(__dirname);
console.log(__filename);

console.log(b1)