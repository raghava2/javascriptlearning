const inputArray = [1,1,1,5,2,7,0];
let length = inputArray.length;

const outputArray = new Array(length);
console.log(outputArray);

//n^2 time complexity solution 

for (let j = 0; j < length; j++) {
    let count = 0;
for (let i = 0; i < length; i++) {
    if(inputArray[i]==j){
        console.log(j + " is encountered.")
        count++;
    }
}
outputArray[j] = count;
console.log("Value : " + j + " appears " + count+ " times.")
}

console.log(outputArray.join(", "));

//n time complexity solution 

const outputArrayA = new Array(length).fill(0);
for (let i = 0; i < length; i++) {
    if(inputArray[i]<length){
        console.log(inputArray[i] + " is encountered : " + ((outputArrayA[(inputArray[i])])+1) + " times")
        outputArrayA[(inputArray[i])] = (outputArrayA[(inputArray[i])])+1; 
    }
}