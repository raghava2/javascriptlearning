const bricks = [9,8,7,3,2,1];
let wallLength = 29;
let counter =0;
let finalPattern = "";
while(wallLength>0){
    let stringPattern = "";
    let firstNum = bricks.shift();
    counter = Math.floor(wallLength/firstNum)+counter;
    stringPattern = stringPattern.concat("".concat(firstNum));
    finalPattern = finalPattern.concat(stringPattern.repeat(Math.floor(wallLength/firstNum)));
    wallLength = wallLength%firstNum;
}
console.log("number of bricks used will be "+counter);
console.log("following bricks will be used in the wall "+ finalPattern);